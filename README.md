# Shipido

An Inventory Management platform built with MERN stack


## [Live Application](https://volunteer-network-283a3.web.app/)


## [Server Side Repo](https://gitlab.com/practice-files-recaps/volunteer-network-server/-/tree/master)

![App Screenshot 1](n2.png)
![App Screenshot 2](n3.png)
![App Screenshot 3](n4.png)
![App Screenshot 4](n5.png)
![App Screenshot 5](n6.png)
![App Screenshot 6](n7.png)
![App Screenshot 7](n1.png)


## Tech Stack

**Fronted:** 
- React Js (create-react-app)
- Javascript
- React bootstrap

**Backend:** 
- Node Js
- Express Js
- MongoDB
- JWT Authentication 

**Hosting:**
- Vercel
- Firebase

## Features

- Fully functional Authentication (Login, Signup, Verification)
- JWT Authentication
- People can find various volunteer work.
- People can register themselves to volunteer.
- Event add, Booking for Event, Individual registered event, Volunteer Register List

  
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/practice-files-recaps/volunteer-network-client.git
```

Go to the project directory

```bash
  cd volunteer-network-client
```

Install dependencies

```bash
  npm install
```

  
## Sample login credentials:

```bash
diyeben617@eilnews.com
123456
```

